# FrontendBlocks

## 一软在手，前端无忧！超强FLEX布局工具！

## 简介
本软件是一款强大的所见即所得前端页面设计器，是低代码开发领域的基础建设，生成的代码不依赖于任何框架，实测可以将前端布局工作的耗时减少80%以上，最关键的是，它实现了人人都可以写前端页面的梦想。  
不用写一行代码即可让设计师、前端开发人员轻松设计出前端代码的利器，100%还原设计稿不再是梦。对于Uniapp开发非常友好，可以快速生成组件和页面。  
本软件可以生成HTML5代码、按2x生成手机端VUE代码、按1x生成电脑端VUE代码，也可以直接生成JSON串，供其他开发工具（包括自主研发的）进行二次加工。  
本软件采用Apache 2.0协议开源，可以免费商用。如有任何问题欢迎反馈，让我们共同建设好该开源项目。突出代码贡献者将会把名字写入README.md中，欢迎贡献。

## 快速上手
在本项目右侧发行版里下载ZIP包，本地用VSCode的Live Server直接运行即可。  
发行版包里有一些简单的设计稿范例可供快速验证。  
正式环境部署放到任意Nginx、IIS或者Apache里都可以跑。

## 最近更新日志

写在前面：因为这个项目纯为爱发电，本人没有从本项目获得任何收入，所以只能靠各位大佬的996来撑起这个项目，帮忙测测也是爱。有需要付费定制集成需求的朋友也可以联系我。

2024年07月31日（4.0版本）：
1. 重磅升级提供Uniapp-X的uvue代码生成工具，全面拥抱安卓、iOS、鸿蒙系统的生态。

2024年03月31日（3.9版本）：
1. 智能父子连字符关联：支持CSS名称连字符的父子关系关联，生成产物的命名更加省心
   我们可以用连字符命名各个块，并且支持在块中嵌套块，这样我们就可以很方便的实现复杂的页面布局。
   比如icon-btn，下面分别有icon-btn-image和icon-btn-text两个子元素，
   那么在生成的时候会自动生成下面的这种代码：
```html
<div class="icon-btn" onclick="iconBtnClick()">
    <img class="image"/>
    <div class="text">图标文字</div>
</div>
<style>
    .icon-btn{
        box-sizing:border-box;
        border-bottom:1px solid rgba(229, 229, 229, 1);
        border-right:1px solid rgba(229, 229, 229, 1);
        text-align:left;
        display:flex;
        position:relative;
        height:92px;
        width:33%;
        flex-direction:column;
        justify-content:center;
        align-items:center;
    }
    .icon-btn{
        user-select: none;
    }
    .icon-btn:hover{
        opacity: 0.8;
    }
    .icon-btn:active{
        opacity: 0.5;
    }
    .icon-btn>.image{
        background-image:url(https://www.yumeisoft.com/demo-icon.jpg);
        background-size:cover;
        background-repeat:no-repeat;
        background-position:center;
        text-align:left;
        width:32px;
        height:32px;
    }
    .icon-btn>.text{
        font-size:14px;
        color:rgba(102, 102, 102, 1);
        text-align:left;
        display:flex;
        position:relative;
        width:100%;
        flex-direction:column;
        align-items:center;
    }
</style>
```
2. 智能CSS精简：精简了生成的CSS代码，原来数千行的代码现如今只需几十行。对于结构相同并且所有子元素样式一模一样的，不会再生成新的block样式，而是直接复用之前的样式。
3. 对于名称为-btn、-button结尾的元素自动加上点击效果，并且自动添加点击事件，写业务代码更方便，生成完几乎直接就可以开写！
4. 增加了7个常用模板，可以更快速的构建布局。
5. 极速设置面板里增加了一键“设置随机占位图”按钮，立即就能给当前块加背景，快速获得图文并茂的效果。
6. 重写了导出vue和html的代码，现在导出规则已经和ToolsBar解耦，你可以更方便的写你想要的任何导出规则。
7. 导出界面增加了一键复制功能，粘贴到代码里就能用！更贴心。

历史更新日志：

2023年02月19日（3.7版本）：增加了大家非常急需的Ctrl+Z撤销功能，调整了一下模板，给一个后台模板和表单模板

#### 软件美照
![alt](https://gitcode.net/yumeisoft/frontend-blocks/-/raw/master/doc/preview1.png)
![alt](https://gitcode.net/yumeisoft/frontend-blocks/-/raw/master/doc/preview2.png)
![alt](https://gitcode.net/yumeisoft/frontend-blocks/-/raw/master/doc/preview3.png)
![alt](https://gitcode.net/yumeisoft/frontend-blocks/-/raw/master/doc/preview4.png)

在线演示地址：
Online presentation address:
http://39.104.17.92/frontendBlocks/

#### 软件架构
使用VUE2.0开发，开箱即用。所生成的代码采用Flex布局和流式布局，具备良好的自适应性，特殊需求下也可以支持绝对定位布局、固钉布局。  
对于Uniapp移动端开发者而言，可以直接生成rpx单位的样式。  
对于图片，建议自建图床进行维护，如果有图片素材希望随着设计稿而维护，可以上网搜索“图片转BASE64编码”，把编码当做图片地址粘过去也是一样的（生成的文件会较大）。  

#### 安装教程

1.  npm install  
2.  npm run serve  
3.  用浏览器打开 http://localhost:9000/frontendBlocks/

#### 独立部署教程

1.  npm run build
2.  把dist目录中的文件拷贝到服务器上，部署目录为frontendBlocks即可(copy files in 'dist' directory to your sever, deploy in apache/nginx and folder called 'frontendBlocks')

#### 使用说明

1.  左侧图层面板可以点击“添加”按钮添加子元素
2.  设计时高度和设计时内距用来方便观察父子元素之间的关系，
3.  左侧面板的“在内部插入”功能是来源于public下的tools.json，其中填写的blocks节点源于“代码生成”菜单下的“生成可二次加工的JSON串”
4.  中间面板可以调整自适应宽度，如果是设计手机端，建议使用375px
5.  右侧面板当点选某个块时可以调整其属性，高级面板里可以设置元素类型的表达，支持表达成input
6.  文件可以新建、保存、读取，可以跨页签复制粘贴（复用设计稿的利器）
7.  当前的设计会被自动保存，待下次打开时可继续上次的设计稿继续设计，非常的人性化
8.  点击块上的小加号，可以很方便的追加新的块

#### 疑难解答
1.  布局用不明白怎么办？  
建议使用者先了解一下Flex布局。如果没有Flex布局基础可以先从这几步开始：设置父容器的“子元素排列”->设置子元素的宽度和高度->如果子元素的宽度和高度是百分比，则需要设置父容器的宽度和高度->给子元素设置一些可视化的效果，比如背景颜色、文本内容等等
2. 元素排列太紧密了，点不上怎么办？  
可以把“设计时高度”和“设计时内距”拉大一点点，这样就能看到元素了，这两个设置不会影响输出效果。而且强烈建议在左侧“图层”面板中进行元素的点选，这样会更加精确
3. 为什么我设置了宽度或高度但实际上不是按我设置的值显示？而且为什么当我添加元素多起来的时候，所有元素都挤在一起了？  
这是因为Flex布局下元素呈弹性效果，如果要保持一个元素固定宽度或高度，另一个元素自适应，请在固定宽高的元素上勾选“布局”->“弹性”->“钢化”。如果要实现一个可滚动的列表区，请将父元素勾选“布局”->“弹性”->“关闭弹性”
4. 有的元素里面子元素过多，导致页面出现滚动条或者影响了其他元素的布局怎么办？  
可以尝试设置“高级”->“子元素溢出设置”->“滚动条”，只要不是“未设置”，都会触发BFC机制，成为独立渲染区域，这样就不会出现溢出或影响其他元素布局的情况了。
5. 我的设计稿崩溃导致FrontendBlocks打不开，怎么办？  
尽管我极力避免软件出现崩溃，但事实上软件操作环境复杂，可能出现无法预料的BUG。可以按下F12，在应用（Application）->存储（Storage）->本地存储空间（Local Storage）中，把tempSave删掉，重新刷新界面即可。如果不涉密的话，可以将tempSave中的值复制出来提一个Issue，社区会尽快解决这个BUG。

#### 二次开发说明
1.  “在内部插入”功能的扩展是在public/tools.json中完成，手动编写可以参照第4条。
2.  想要增加新的属性，可以修改src/components/layout/PropertiesPanel.vue文件，data里的defaultForm是默认值，每一次更新都会用它来初始化，当前编辑的元素样式都会存在在propForm对象里。fromBlock方法会将选中元素的样式进行解析，回显到界面上。而toBlock方法则会将填写好的属性赋予给选中元素，使之生效。（如果你需要和业务系统相结合，可以在这里加点东西，比如接口地址、数据来源之类的）
3.  生成代码的功能都放在了src/components/layout/ToolsBar.vue里，想生成其他语言的（比如Android的XML、React、甚至Winform，可以根据逻辑自行实现）
4.  每一个元素所对应的Object里，properties代表了附加属性，id代表全局唯一编码，style代表该元素的样式，blocks代表该元素的子元素集合，domType是元素的类型（默认是div，即使表达成view也是写成div），text是指元素内部的文字
5.  上下文在src/components/layout/WorkSpace.vue里，它是充血的，可以扩展方法，而且几乎在整个项目的任何地方都能找到它的引用，其中的createNewBlock定义了创建一个新元素的初始属性。
6.  总的来说，实现逻辑就是：模板创建->用户自定义属性->上下文连锁更新->Block.vue预览表达->ToolsBar.vue生成代码->CodePreview.vue预览并下载代码

#### 快捷键
1.  Ctrl+C 复制选中的块
2.  Ctrl+V 在所选中块的内部进行追加粘贴
3.  Delete 删除选中的块
4.  Ctrl+Z 撤销

#### 参与贡献

1.  黑龙江省瑜美科技发展有限公司 杨若瑜 2020年 创建 1.0

#### 其他

1.  我的博客：yangruoyu.blog.csdn.net
